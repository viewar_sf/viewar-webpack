module.exports = {
  extends: ['viewar/env/react'],
  rules: {
    semi: ['error', 'never'],
    "react/jsx-filename-extension": 0
  }
}
